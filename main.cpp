
#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#define VERSION 20080804

static float angleX, angleY, cubeAngle;
static int mouseX, mouseY;
static bool rotate;

void init()
{
    glClearColor (0.0, 0.0, 0.0, 0.0);

	glEnable(GL_DEPTH_TEST);

	// Setup lighting
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// Setup shading
    glShadeModel (GL_SMOOTH);
}

void lighting()
{
	GLfloat position[] = { 2.0, 3.0, -1.0, 1.0 };
	GLfloat lightColor[] = { 1.0, 1.0, 1.0 }; 
	GLfloat ambientColor[] = { 1.0, 1.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, position);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightColor);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightColor);


}

void display() 
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glColor4f(0.0, 1.0, 0.0, 1.0);
    glLoadIdentity();
    gluLookAt (0.0, 2.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	lighting();

	glPushMatrix();
    glRotatef(angleX, 0.0, 1.0, 0.0);
    glRotatef(angleY, 1.0, 0.0, 0.0);
	glFrontFace(GL_CW);
    glutSolidTeapot (1.0);
	glFrontFace(GL_CCW);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0, 2.0, 0.0);
    glRotatef(cubeAngle, 0.0, 1.0, 0.0);
	glRotatef(30, 1.0, 0, 0);
	glColor3f(0.0, 0.0, 1.0);
	glutSolidCube(0.5);
	glPopMatrix();

	glutSwapBuffers(); 
}


void reshape (int w, int h)
{
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float)w/h, 1.0, 20.0);
    glMatrixMode (GL_MODELVIEW);
}

void mouse (int button, int state, int x, int y) 
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
		rotate = true;
	}
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		rotate = false;
	}
    mouseX = x;
	mouseY = y;
}

void mousePos(int x, int y)
{
	if (rotate) 
	{
		angleX += 1 * (x - mouseX);
		angleY += 1 * (y - mouseY);
		glutPostRedisplay();
	}
    mouseX = x;
	mouseY = y;
}

void update(int value)
{
	cubeAngle++;
	if (cubeAngle > 360) cubeAngle -= 360;	
	glutPostRedisplay();
	glutTimerFunc(20, update, 1);
}


int main(int argc, char** argv)
{
    std::cout << "Spatial Auditory Memory Experiment" << std::endl;

    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (500, 500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Spatial Auditory Memory Experiment");
    init();
    glutDisplayFunc (display);
    glutReshapeFunc (reshape);
	glutTimerFunc(1, update, 1);

    glutMouseFunc(mouse);
	glutMotionFunc(mousePos);

    glutMainLoop();
    return 0;
}
