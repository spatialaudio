CXX = g++
CCFLAGS = -g -O3 -Wall -lpthread -lglut -lGL -lGLU -lm -lX11 -lXext

all: main

main: main.o
	${CXX} ${CCFLAGS} main.o -o main

main.o: main.cpp
	${CXX} -c main.cpp

clean:
	rm main
	rm main.o
